export type TUser = {
  id: number,
  name?: string,
  username: string,
  email: string,
  website: string,
  company: Company,
  nb_todos?: number,
  nb_albums?: number
}

type Company = {
  name: string
}

export type Album = {
  id?: number,
  userId?: number,
  title: string,
}

export type AlbumPhoto = {
  id: number,
  title: string,
  url: string,
  thumbnailUrl: string
}