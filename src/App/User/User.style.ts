import styled from 'styled-components';

export const UserContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 250px;
  padding: 20px;
  margin-bottom: 20px;

  box-shadow: 2px 2px 6px rgba(0, 0, 0, 0.1);
`;