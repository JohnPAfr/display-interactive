import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { UserContainer } from './User.style'

const User = ({ user }: { user: any }) => {
  const [data, setData] = useState(user[1]);
  const [todos, setTodos] = useState(0);
  const [albums, setAlbums] = useState(0);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    setData(user[1]);
    data.nb_todos.then((res: number) => setTodos(res));
    data.nb_albums.then((res: number) => setAlbums(res));
    setLoading(false);
  }, [data, user]);
  
  return (
    <UserContainer>
      <Link to={`users/${data?.id}`}>{data?.username}</Link>
      <div>{data?.email}</div>
      <a href={data?.website} target="_blank" rel="noreferrer">{data?.website}</a>
      <div>{data?.company_name}</div>
      <div>{data?.username}</div>
      { 
        loading ? <p>Loading...</p> 
        : <>
            <div>todos {todos}</div>
            <div>albums {albums}</div>
          </>
      }
    </UserContainer>
  )
}

export default User