import React, { useEffect, useState } from 'react';
import { Route, Switch } from 'react-router-dom';
import { AppContainer } from './App.style';
import Users from './Users';
import axios from '../utils/axios';
import { AxiosResponse } from 'axios';
import UserProfil from './UserProfil';
import UserAlbum from './UserAlbums/UserAlbum';

function App() {

  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    (async function() {
      setLoading(true);
      const reqUsers: AxiosResponse = await axios.get('users');
      setUsers(await reqUsers.data); 
      setLoading(false);     
    })()
  }, [])

  return (
    <AppContainer className="App">      
      <Switch>
        <Route path='/' exact>
          <Users users={users} loading={loading}/>
        </Route>
        <Route path='/users/:userid/:albumid'>
          <UserAlbum />
        </Route>
        <Route path='/users/:userid'>
          <UserProfil />
        </Route>
      </Switch>
    </AppContainer>
  );
}

export default App;
