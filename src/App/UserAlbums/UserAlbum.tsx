import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { AlbumPhotos, UserAlbumContainer } from './UserAlbum.style';
import axios from '../../utils/axios';
import { Album, AlbumPhoto } from '../../model/model';
import { AxiosResponse } from 'axios';

const UserAlbum = () => {
  const { userid, albumid } = useParams<{userid: string, albumid: string}>();
  const [album, setAlbum] = useState<Album>();
  const [albumPhotos, setAlbumPhotos] = useState<AlbumPhoto[]>();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    (async function(){
      setLoading(true);
      const album_req: AxiosResponse = await axios.get('albums/' + albumid);
      setAlbum(await album_req.data);
      const album_photos_req: AxiosResponse = await axios.get('photos?albumId=' + albumid);
      setAlbumPhotos(await album_photos_req.data);
      setLoading(true);
    })()
  }, [userid, albumid]);
  
  return (
    <UserAlbumContainer>
      <Link to={`/users/${userid}`} className="btn">Go back to user</Link>
      { loading ? <p>Loading...</p>
        : <>
            <h1>{album?.title}</h1>
            <AlbumPhotos>
              {albumPhotos && albumPhotos.map((photo: AlbumPhoto) => <img src={photo.thumbnailUrl} alt={photo.title}/>)}
            </AlbumPhotos>
          </>
      }
    </UserAlbumContainer>
  )
}

export default UserAlbum