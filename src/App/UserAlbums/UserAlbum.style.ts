import styled from 'styled-components';

export const UserAlbumContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 900px;
  margin: 0 auto;
  margin-bottom: 20px;
`;

export const AlbumPhotos = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
  align-items: center;

  img {
    display: block;
    width: 150px;
    height: 150px;
  }
`