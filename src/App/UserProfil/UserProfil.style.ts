import styled from 'styled-components';

export const UserProfilContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  height: 100%;

  padding: 20px;
  margin-bottom: 20px;
`;

export const UserProfilAlbums = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;

  margin-top: 20px;
`