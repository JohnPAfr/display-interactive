import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'
import { UserProfilAlbums, UserProfilContainer } from './UserProfil.style'
import { AxiosResponse } from 'axios';
import axios from '../../utils/axios'
import { Album, TUser } from '../../model/model';

const User = () => {
  const { userid } = useParams<{userid: string}>();
  const [userData, setUserData] = useState<TUser>();
  const [albums, setAlbums] = useState<Album[]>();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    (async function() {
      setLoading(true);
      const user: AxiosResponse = await axios.get('users/' + userid);
      setUserData(await user.data);
      const albums_req: AxiosResponse = await axios.get('users/' + userid + '/albums');
      setAlbums(await albums_req.data);      
      setLoading(false);
    })()
  }, [userid])
  
  return (
    <UserProfilContainer>
      <Link to="/" className="btn">Go back to users</Link>
      { 
        loading ? <p>Loading...</p>
        : <>
            <h1>{userData?.name}</h1>
            <h2>{userData?.username}</h2>
            <p>{userData?.email}</p>
            <UserProfilAlbums>
              <h3>Titles</h3>
              {albums?.map((album: Album) => <Link key={album?.id} to={`/users/${userid}/${album?.id}`}>{ album?.title }</Link>)}
            </UserProfilAlbums>
          </>
      }
    </UserProfilContainer>
  )
}

export default User