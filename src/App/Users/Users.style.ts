import styled from 'styled-components';

export const UsersContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;

  max-width: 900px;
  margin: 0 auto;
`