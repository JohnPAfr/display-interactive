import React, { useEffect, useState } from 'react'
import User from '../User/User'
import { UsersContainer } from './Users.style'
import axios from '../../utils/axios'
import { AxiosResponse } from 'axios'
import { TUser } from '../../model/model'

const Users = ({ users, loading }: { users: TUser[], loading: boolean }) => {

  const [formattedUsers, setFormattedUsers] = useState<any[]>();

  useEffect(() => {
    const newUsers = users.map((currentUser: TUser) => {
      const nb_todos_req: Promise<AxiosResponse> = axios.get(`todos?userId=${currentUser.id}`);
      const nb_todos = nb_todos_req.then(res => res.data).then(data => data.length);
      const nb_albums_req: Promise<AxiosResponse> = axios.get(`users/${currentUser.id}/albums`);
      const nb_albums = nb_albums_req.then(res => res.data.length); 
      
      return {
        ...currentUser,
        website: `www.${currentUser?.website}`,
        nb_todos: nb_todos,
        nb_albums: nb_albums
      }
    });
    setFormattedUsers(Object.entries(newUsers));    
  }, [users])

  return (
    <UsersContainer>
      { 
        loading ? <p>Loading...</p> 
        : (formattedUsers && formattedUsers.map(user => <User key={user.id} user={user} />))
      }
    </UsersContainer>
  )
}

export default Users
